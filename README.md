# Documented demos of the `mgis.fenics` module

This repository contains a collection of documented demos illustrating the use of the FEniCS/MFront interface implemented in the `mgis.fenics` module of the [MGIS project](https://github.com/thelfer/MFrontGenericInterfaceSupport).

## Usage

Demos are written in Jupyter notebook formats and located in the `demos` directory along with MFront source files.

Python versions of the demos are also provided.

## Documentation

Online versions of the demos are found on the [MGIS website](https://thelfer.github.io/mgis/web/index.html).
A general description of the `mgis.fenics` module and a list of available demos is [accessible here](https://thelfer.github.io/mgis/web/mgis_fenics.html).

Html versions of the demos can also be generated inside this repository by running the Python script `docs/process_sources.py`.