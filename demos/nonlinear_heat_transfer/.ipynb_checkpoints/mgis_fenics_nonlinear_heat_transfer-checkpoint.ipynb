{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Stationnary non-linear heat transfer\n",
    "\n",
    "## Description of the non-linear constitutive heat transfer law\n",
    "\n",
    "The thermal material is described by the following non linear Fourier\n",
    "Law:\n",
    "\n",
    "$$\n",
    "\\mathbf{j}=-k\\left(T\\right)\\,\\mathbf{\\nabla} T\n",
    "$$\n",
    "\n",
    "where $\\mathbf{j}$ is the heat flux and $\\mathbf{\\nabla} T$ is the\n",
    "temperature gradient.\n",
    "\n",
    "### Expression of the thermal conductivity\n",
    "\n",
    "The thermal conductivity is assumed to be given by:\n",
    "\n",
    "$$\n",
    "k\\left(T\\right)={\\displaystyle \\frac{\\displaystyle 1}{\\displaystyle A+B\\,T}}\n",
    "$$\n",
    "\n",
    "This expression accounts for the phononic contribution to the thermal\n",
    "conductivity.\n",
    "\n",
    "### Derivatives\n",
    "\n",
    "As discussed below, the consistent linearisation of the heat transfer\n",
    "equilibrium requires to compute:\n",
    "\n",
    "-   the derivative\n",
    "    ${\\displaystyle \\frac{\\displaystyle \\partial \\mathbf{j}}{\\displaystyle \\partial \\mathbf{\\nabla} T}}$\n",
    "    of the heat flux with respect to the temperature gradient.\n",
    "    ${\\displaystyle \\frac{\\displaystyle \\partial \\mathbf{j}}{\\displaystyle \\partial \\mathbf{\\nabla} T}}$\n",
    "    is given by: $$\n",
    "      {\\displaystyle \\frac{\\displaystyle \\partial \\mathbf{j}}{\\displaystyle \\partial \\mathbf{\\nabla} T}}=-k\\left(T\\right)\\,\\matrix{I}\n",
    "    $$\n",
    "-   the derivative\n",
    "    ${\\displaystyle \\frac{\\displaystyle \\partial \\mathbf{j}}{\\displaystyle \\partial T}}$\n",
    "    of the heat flux with respect to the temperature.\n",
    "    ${\\displaystyle \\frac{\\displaystyle \\partial \\mathbf{j}}{\\displaystyle \\partial T}}$\n",
    "    is given by: $$\n",
    "      {\\displaystyle \\frac{\\displaystyle \\partial \\mathbf{j}}{\\displaystyle \\partial T}}=-{\\displaystyle \\frac{\\displaystyle \\partial k\\left(T\\right)}{\\displaystyle \\partial T}}\\,\\mathbf{\\nabla} T=B\\,k^{2}\\,\\mathbf{\\nabla} T\n",
    "    $$\n",
    "\n",
    "## `MFront`’ implementation\n",
    "\n",
    "### Choice of the the domain specific language\n",
    "\n",
    "Every `MFront` file is handled by a domain specific language (DSL), which\n",
    "aims at providing the most suitable abstraction for a particular choice\n",
    "of behaviour and integration algorithm. See `mfront mfront --list-dsl`\n",
    "for a list of the available DSLs.\n",
    "\n",
    "The name of DSL’s handling generic behaviours ends with\n",
    "`GenericBehaviour`. The first part of a DSL’s name is related to the\n",
    "integration algorithm used.\n",
    "\n",
    "In the case of this non linear transfer behaviour, the heat flux is\n",
    "explicitly computed from the temperature and the temperature gradient.\n",
    "The `DefaultGenericBehaviour` is the most suitable choice:\n",
    "\n",
    "``` cpp\n",
    "@DSL DefaultGenericBehaviour;\n",
    "```\n",
    "\n",
    "### Some metadata\n",
    "\n",
    "The following lines define the name of the behaviour, the name of the\n",
    "author and the date of its writing:\n",
    "\n",
    "``` cpp\n",
    "@Behaviour StationaryHeatTransfer;\n",
    "@Author Thomas Helfer;\n",
    "@Date 15/02/2019;\n",
    "```\n",
    "\n",
    "### Gradients and fluxes\n",
    "\n",
    "Generic behaviours relate pairs of gradients and fluxes. Gradients and\n",
    "fluxes are declared independently but the first declared gradient is\n",
    "assumed to be conjugated with the first declared fluxes and so on…\n",
    "\n",
    "The temperature gradient is declared as follows (note that Unicode characters are supported):\n",
    "\n",
    "``` cpp\n",
    "@Gradient TemperatureGradient ∇T;\n",
    "∇T.setGlossaryName(\"TemperatureGradient\");\n",
    "```\n",
    "\n",
    "Note that we associated to `∇T` the glossary name `TemperatureGradient`.\n",
    "This is helpful for the calling code.\n",
    "\n",
    "After this declaration, the following variables will be defined:\n",
    "\n",
    "-   The temperature gradient `∇T` at the beginning of the time step.\n",
    "-   The increment of the temperature gradient `Δ∇T` over the time step.\n",
    "\n",
    "The heat flux is then declared as follows:\n",
    "\n",
    "``` cpp\n",
    "@Flux HeatFlux j;\n",
    "j.setGlossaryName(\"HeatFlux\");\n",
    "```\n",
    "\n",
    "In the following code blocks, `j` will be the heat flux at the end of\n",
    "the time step.\n",
    "\n",
    "### Tangent operator blocks\n",
    "\n",
    "By default, the derivatives of the gradients with respect to the fluxes\n",
    "are declared. Thus the variable `∂j∕∂Δ∇T` is automatically declared.\n",
    "\n",
    "However, as discussed in the next section, the consistent linearisation\n",
    "of the thermal equilibrium requires to return the derivate of the heat\n",
    "flux with respect to the increment of the temperature (or equivalently\n",
    "with respect to the temperature at the end of the time step).\n",
    "\n",
    "``` cpp\n",
    "@AdditionalTangentOperatorBlock ∂j∕∂ΔT;\n",
    "```\n",
    "\n",
    "### Parameters\n",
    "\n",
    "The `A` and `B` coefficients that appears in the definition of the\n",
    "thermal conductivity are declared as parameters:\n",
    "\n",
    "``` cpp\n",
    "@Parameter real A = 0.0375;\n",
    "@Parameter real B = 2.165e-4;\n",
    "```\n",
    "\n",
    "Parameters are stored globally and can be modified from the calling\n",
    "solver or from `python` in the case of the coupling with `FEniCS`\n",
    "discussed below.\n",
    "\n",
    "### Local variable\n",
    "\n",
    "A local variable is accessible in each code blocks.\n",
    "\n",
    "Here, we declare the thermal conductivity `k` as a local variable in\n",
    "order to be able to compute its value during the behaviour integration\n",
    "and to reuse this value when computing the tangent operator.\n",
    "\n",
    "``` cpp\n",
    "@LocalVariable thermalconductivity k;\n",
    "```\n",
    "\n",
    "### Integration of the behaviour\n",
    "\n",
    "The behaviour integration is straightforward: one starts to compute the\n",
    "temperature at the end of the time step, then we compute the thermal\n",
    "conductivity (at the end of the time step) and the heat flux using the\n",
    "temperature gradient (at the end of the time step).\n",
    "\n",
    "``` cpp\n",
    "@Integrator{\n",
    "  // temperature at the end of the time step\n",
    "  const auto T_ = T + ΔT;\n",
    "  // thermal conductivity\n",
    "  k = 1 / (A + B ⋅ T_);\n",
    "  // heat flux\n",
    "  j = -k ⋅ (∇T + Δ∇T);\n",
    "} // end of @Integrator\n",
    "```\n",
    "\n",
    "### Tangent operator\n",
    "\n",
    "The computation of the tangent operator blocks is equally simple:\n",
    "\n",
    "``` cpp\n",
    "@TangentOperator {\n",
    "  ∂j∕∂Δ∇T = -k ⋅ tmatrix<N, N, real>::Id();\n",
    "  ∂j∕∂ΔT  =  B ⋅ k ⋅ k ⋅ (∇T + Δ∇T);\n",
    "} // end of @TangentOperator \n",
    "```\n",
    "## `FEniCS` implementation\n",
    "\n",
    "We consider a rectanglar domain with imposed temperatures `Tl` (resp. `Tr`) on the left (resp. right) boundaries. We want to solve for the temperature field `T` inside the domain using a $P^1$-interpolation. We initialize the temperature at value `Tl` throughout the domain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "import matplotlib.pyplot as plt\n",
    "from dolfin import *\n",
    "import mgis.fenics as mf\n",
    "\n",
    "length = 30e-3\n",
    "width = 5.4e-3\n",
    "mesh = RectangleMesh(Point(0., 0.), Point(length, width), 100, 10)\n",
    "\n",
    "V = FunctionSpace(mesh, \"CG\", 1)\n",
    "T = Function(V, name=\"Temperature\")\n",
    "\n",
    "def left(x, on_boundary):\n",
    "    return near(x[0], 0) and on_boundary\n",
    "def right(x, on_boundary):\n",
    "    return near(x[0], length) and on_boundary\n",
    "\n",
    "Tl = 300\n",
    "Tr = 800\n",
    "T.interpolate(Constant(Tl))\n",
    "\n",
    "bc = [DirichletBC(V, Constant(Tl), left),\n",
    "      DirichletBC(V, Constant(Tr), right)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Loading the material behaviour\n",
    "\n",
    "We use the `MFrontNonlinearMaterial` class for describing the material behaviour. The first argument corresponds to the path where material librairies have been compiled, the second correspond to the name of the behaviour (declared with `@Behaviour`). Finally, the modelling hypothesis is specified (default behaviour is `\"3d\"`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Behaviour 'StationaryHeatTransfer' has not been found in 'src/libBehaviour.so'.\n",
      "Attempting to compile 'StationaryHeatTransfer.mfront' in '/'...\n"
     ]
    },
    {
     "ename": "RuntimeError",
     "evalue": "LibrariesManager::loadLibrary: library 'src/libBehaviour.so' could not be loaded, (src/libBehaviour.so: cannot open shared object file: No such file or directory)",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mRuntimeError\u001b[0m                              Traceback (most recent call last)",
      "\u001b[0;32m/usr/local/lib/python3.6/site-packages/mgis/fenics/nonlinear_material.py\u001b[0m in \u001b[0;36m__init__\u001b[0;34m(self, path, name, hypothesis, material_properties, parameters)\u001b[0m\n\u001b[1;32m     52\u001b[0m         \u001b[0;32mtry\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 53\u001b[0;31m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mload_behaviour\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     54\u001b[0m         \u001b[0;32mexcept\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/usr/local/lib/python3.6/site-packages/mgis/fenics/nonlinear_material.py\u001b[0m in \u001b[0;36mload_behaviour\u001b[0;34m(self)\u001b[0m\n\u001b[1;32m     65\u001b[0m     \u001b[0;32mdef\u001b[0m \u001b[0mload_behaviour\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 66\u001b[0;31m         \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mis_finite_strain\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mmgis_bv\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0misStandardFiniteStrainBehaviour\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mpath\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mname\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     67\u001b[0m         \u001b[0;32mif\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mis_finite_strain\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mRuntimeError\u001b[0m: LibrariesManager::loadLibrary: library 'src/libBehaviour.so' could not be loaded, (src/libBehaviour.so: cannot open shared object file: No such file or directory)",
      "\nDuring handling of the above exception, another exception occurred:\n",
      "\u001b[0;31mRuntimeError\u001b[0m                              Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-2-00cb4487a402>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      1\u001b[0m material = mf.MFrontNonlinearMaterial(\"src/libBehaviour.so\",\n\u001b[1;32m      2\u001b[0m                                       \u001b[0;34m\"StationaryHeatTransfer\"\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 3\u001b[0;31m                                       hypothesis=\"plane_strain\")\n\u001b[0m",
      "\u001b[0;32m/usr/local/lib/python3.6/site-packages/mgis/fenics/nonlinear_material.py\u001b[0m in \u001b[0;36m__init__\u001b[0;34m(self, path, name, hypothesis, material_properties, parameters)\u001b[0m\n\u001b[1;32m     60\u001b[0m             \u001b[0msubprocess\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mrun\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;34m\"mfront\"\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m\"--obuild\"\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m\"--interface=generic\"\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mname\u001b[0m\u001b[0;34m+\u001b[0m\u001b[0;34m\".mfront\"\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     61\u001b[0m             \u001b[0mos\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mchdir\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mcwd\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 62\u001b[0;31m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mload_behaviour\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     63\u001b[0m         \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mupdate_parameters\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mparameters\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     64\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/usr/local/lib/python3.6/site-packages/mgis/fenics/nonlinear_material.py\u001b[0m in \u001b[0;36mload_behaviour\u001b[0;34m(self)\u001b[0m\n\u001b[1;32m     64\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     65\u001b[0m     \u001b[0;32mdef\u001b[0m \u001b[0mload_behaviour\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 66\u001b[0;31m         \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mis_finite_strain\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mmgis_bv\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0misStandardFiniteStrainBehaviour\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mpath\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mname\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     67\u001b[0m         \u001b[0;32mif\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mis_finite_strain\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     68\u001b[0m             \u001b[0;31m# finite strain options\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mRuntimeError\u001b[0m: LibrariesManager::loadLibrary: library 'src/libBehaviour.so' could not be loaded, (src/libBehaviour.so: cannot open shared object file: No such file or directory)"
     ]
    }
   ],
   "source": [
    "material = mf.MFrontNonlinearMaterial(\"./src/libBehaviour.so\",\n",
    "                                      \"StationaryHeatTransfer\",\n",
    "                                      hypothesis=\"plane_strain\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `MFront` behaviour declares the field `\"TemperatureGradient\"` as a Gradient variable, with its associated Flux called `\"HeatFlux\"`. We can check that the `material` object retrieves `MFront`'s gradient and flux names, as well as the different tangent operator blocks which have been defined, namely `dj_ddgT` and `dj_ddT` in the present case:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(material.get_gradient_names())\n",
    "print(material.get_flux_names())\n",
    "print([\"d{}_d{}\".format(*t) for t in material.get_tangent_block_names()])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Non-linear problem definition\n",
    "\n",
    "When defining the non-linear problem, we will specify the boundary conditions and the requested quadrature degree which will control the number of quadrature points used in each cell to compute the non-linear constitutive law. Here, we specify a quadrature of degree 2 (i.e. 3 Gauss points for a triangular element). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "problem = mf.MFrontNonlinearProblem(T, material, quadrature_degree=2, bcs=bc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variable registration\n",
    "\n",
    "The `MFront` behaviour implicitly declares the temperature as an external state variable called `\"Temperature\"`. We must therefore associate this external state variable to a known mechanical field. This can be achieved explicitly using the `register_external_state_variable` method. In the present case, this can be done automatically since the name of the unknown temperature field matches the [TFEL Glossary](http://tfel.sourceforge.net/glossary.html) name `\"Temperature\"` which is a predefined external state variable. In this case, the following message will be printed:\n",
    "```\n",
    "Automatic registration of 'Temperature' as an external state variable.\n",
    "```\n",
    "For problems in which the temperature only acts as a parameter (no jacobian blocks with respect to the temperature), the temperature can be automatically registered as a constant value ($293.15 \\text{ K}$ by default) or to any other (`dolfin.Constant`, `float` or `dolfin.Function`) value using the `register_external_state_variable` method.\n",
    "\n",
    "In the `FEniCS` interface, we instantiate the main mechanical unknown, here the temperature field `T` which has to be named `\"Temperature\"` in order to match `MFront`'s predefined name. Using another name than this will later result in an error saying:\n",
    "```\n",
    "ValueError: 'Temperature' could not be associated with a registered gradient or a known state variable.\n",
    "```\n",
    "Finally, we need to associate to `MFront` gradient object the corresponding UFL expression as a function of the unknown field `T`. To do so, we use the `register_gradient` method linking `MFront` `\"TemperatureGradient\"` object to the UFL expression `grad(T)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "problem.register_gradient(\"TemperatureGradient\", grad(T))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly to the case of external state variables, common gradient expressions for some [TFEL Glossary](http://tfel.sourceforge.net/glossary.html) names have been already predefined which avoid calling explicitly the `register_gradient` method. Predefined expressions can be obtained from:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mf.list_predefined_gradients()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that the name `\"Temperature Gradient\"` is in fact a predefined gradient. Omitting calling the `register_gradient` method will in this case print the following message upon calling `solve`:\n",
    "```\n",
    "Automatic registration of 'TemperatureGradient' as grad(Temperature).\n",
    "```\n",
    "meaning that a predefined gradient name has been found and registered as the UFL expression $\\nabla T$. \n",
    "> Note that automatic registration is not supported when using mixed function spaces.\n",
    "\n",
    "### Problem resolution\n",
    "No external loading has been specified so that the residual form is automatically defined as:\n",
    "\\begin{equation}\n",
    "F(\\widehat{T}) = \\int_\\Omega \\mathbf{j}\\cdot \\nabla \\widehat{T} \\text{dx} = 0 \\quad \\forall \\widehat{T}\n",
    "\\end{equation}\n",
    "\n",
    "From the two tangent operator blocks `dj_ddgT` and `dj_ddT`, it will automatically be deduced that the heat flux $\\mathbf{j}$ is a function of both the temperature gradient $\\mathbf{g}=\\nabla T$ and the temperature itself i.e. $\\mathbf{j}=\\mathbf{j}(\\mathbf{g}, T)$. The following tangent bilinear form will therefore be used  when solving the above non-linear problem:\n",
    "\n",
    "\\begin{equation}\n",
    "a_\\text{tangent}(\\widehat{T},T^*) = \\int_{\\Omega} \\nabla \\widehat{T}\\cdot\\left(\\dfrac{\\partial \\mathbf{j}}{\\partial \\mathbf{g}}\\cdot \\nabla T^*+\\dfrac{\\partial \\mathbf{j}}{\\partial T}\\cdot T^*\\right) \\text{dx}\n",
    "\\end{equation}\n",
    "\n",
    "We finally solve the non-linear problem using a default Newton non-linear solver. The `solve` method returns the number of Newton iterations (4 in the present case) and converged status ."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "problem.solve(T.vector())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We finally check that the thermal conductivity coefficient $k$, computed from the ratio between the horizontal heat flux and temperature gradient matches the temperature-dependent expressions implemented in the `MFront` behaviour."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "j = problem.fluxes[\"HeatFlux\"].function\n",
    "g = problem.gradients[\"TemperatureGradient\"].function\n",
    "k_gauss = -j.vector().get_local()[::2]/g.vector().get_local()[::2]\n",
    "T_gauss = problem.state_variables[\"external\"][\"Temperature\"].function.vector().get_local()\n",
    "A = material.get_parameter(\"A\");\n",
    "B = material.get_parameter(\"B\");\n",
    "k_ref = 1/(A + B*T_gauss)\n",
    "plt.plot(T_gauss, k_gauss, 'o', label=\"FE\")\n",
    "plt.plot(T_gauss, k_ref, '.', label=\"ref\")\n",
    "plt.xlabel(r\"Temperature $T\\: (K)$\")\n",
    "plt.ylabel(r\"Thermal conductivity $k\\: (W.m^{-1}.K^{-1})$\")\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
